<?php
require_once "singleton.class.php";

class Bdd
{
    private $idclient;
    private $nomclient;
    private $prenomclient;
    private $societe;
    private $idrole;
    private $idemploye;
    private $nomemploye;
    private $prenomemploye;
    private $grade;
    private $idoperation;
    private $typeope;
    private $descriptionope;
    private $prix;
    private $identifiant;
    private $idrole_connexion;
    private $mot_de_passe;

    public function client($idclient, $nomclient, $prenomclient, $societe)
    {
        $conn= Singleton::getInstance()->getConnection();
        $resultat = $conn->query('SELECT idclient,nomclient,prenomclient,societe  FROM client');

        $index=0;
        $tab = array();
        //on parcours la tableau $resultats provenant de la bdd ligne par ligne
        //et on recupere les clefs nom et prenom de chaque ligne
        foreach ($resultat as $row) {
            $tab[$index] =  $row['idclient']." ".$row['nomclient'].$row['prenomclient']." ".$row['societe'];
            //echo($tab[$index].PHP_EOL);
            $index++;
        }
    }
    public function employe($idrole, $idemploye, $nomemploye, $prenomemploye, $grade, $identifiant, $idrole_connexion, $mot_de_passe)
    {
        $conn= Singleton::getInstance()->getConnection();
        $resultat = $conn->query('SELECT idrole,idemploye,nomemploye,prenomemploye,identifiant,mot_de_passe,grade  FROM employe');

        $index=0;
        $tab = array();
        //on parcours la tableau $resultats provenant de la bdd ligne par ligne
        //et on recupere les clefs nom et prenom de chaque ligne
        foreach ($resultat as $row) {
            $tab[$index] =  $row['idrole']." ".$row['idemploye']." ".$row['nomemploye']." ".$row['prenomemploye']." ".$row['identifiant']." ".$row['mot_de_passe']." ".$row['grade'];
            //echo($tab[$index].PHP_EOL);
            $index++;
        }
    }
    public function operation($idoperation, $typeope, $idclient, $descriptionope, $prix, $idrole)
    {
        $conn= Singleton::getInstance()->getConnection();
        $resultat = $conn->query('SELECT idoperation,typeope,idclient,descriptionope,prix,idrole  FROM operation');

        $index=0;
        $tab = array();
        //on parcours la tableau $resultats provenant de la bdd ligne par ligne
        //et on recupere les clefs nom et prenom de chaque ligne
        foreach ($resultat as $row) {
            $tab[$index] =  $row['idoperation']." ".$row['typeope']." ".$row['idclient']." ".$row['descriptionope']." ".$row['prix']." ".$row['idrole'];
            //echo($tab[$index].PHP_EOL);
            $index++;
        }
    }
    public function role($idrole, $grade)
    {
        $conn= Singleton::getInstance()->getConnection();
        $resultat = $conn->query('SELECT idrole,grade  FROM role');

        $index=0;
        $tab = array();
        //on parcours la tableau $resultats provenant de la bdd ligne par ligne
        //et on recupere les clefs nom et prenom de chaque ligne
        foreach ($resultat as $row) {
            $tab[$index] =  $row['idrole']." ".$row['grade'];
            //echo($tab[$index].PHP_EOL);
            $index++;
        }
    }

    /**
     * Get the value of idclient
     */ 
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * Set the value of idclient
     *
     * @return  self
     */ 
    public function setIdclient($idclient)
    {
        $this->idclient = $idclient;

        return $this;
    }

    /**
     * Get the value of nomclient
     */ 
    public function getNomclient()
    {
        return $this->nomclient;
    }

    /**
     * Set the value of nomclient
     *
     * @return  self
     */ 
    public function setNomclient($nomclient)
    {
        $this->nomclient = $nomclient;

        return $this;
    }

    /**
     * Get the value of prenomclient
     */ 
    public function getPrenomclient()
    {
        return $this->prenomclient;
    }

    /**
     * Set the value of prenomclient
     *
     * @return  self
     */ 
    public function setPrenomclient($prenomclient)
    {
        $this->prenomclient = $prenomclient;

        return $this;
    }

    /**
     * Get the value of societe
     */ 
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set the value of societe
     *
     * @return  self
     */ 
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get the value of idrole
     */ 
    public function getIdrole()
    {
        return $this->idrole;
    }

    /**
     * Set the value of idrole
     *
     * @return  self
     */ 
    public function setIdrole($idrole)
    {
        $this->idrole = $idrole;

        return $this;
    }

    /**
     * Get the value of idemploye
     */ 
    public function getIdemploye()
    {
        return $this->idemploye;
    }

    /**
     * Set the value of idemploye
     *
     * @return  self
     */ 
    public function setIdemploye($idemploye)
    {
        $this->idemploye = $idemploye;

        return $this;
    }

    /**
     * Get the value of nomemploye
     */ 
    public function getNomemploye()
    {
        return $this->nomemploye;
    }

    /**
     * Set the value of nomemploye
     *
     * @return  self
     */ 
    public function setNomemploye($nomemploye)
    {
        $this->nomemploye = $nomemploye;

        return $this;
    }

    /**
     * Get the value of prenomemploye
     */ 
    public function getPrenomemploye()
    {
        return $this->prenomemploye;
    }

    /**
     * Set the value of prenomemploye
     *
     * @return  self
     */ 
    public function setPrenomemploye($prenomemploye)
    {
        $this->prenomemploye = $prenomemploye;

        return $this;
    }

    /**
     * Get the value of grade
     */ 
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set the value of grade
     *
     * @return  self
     */ 
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get the value of idoperation
     */ 
    public function getIdoperation()
    {
        return $this->idoperation;
    }

    /**
     * Set the value of idoperation
     *
     * @return  self
     */ 
    public function setIdoperation($idoperation)
    {
        $this->idoperation = $idoperation;

        return $this;
    }

    /**
     * Get the value of typeope
     */ 
    public function getTypeope()
    {
        return $this->typeope;
    }

    /**
     * Set the value of typeope
     *
     * @return  self
     */ 
    public function setTypeope($typeope)
    {
        $this->typeope = $typeope;

        return $this;
    }

    /**
     * Get the value of descriptionope
     */ 
    public function getDescriptionope()
    {
        return $this->descriptionope;
    }

    /**
     * Set the value of descriptionope
     *
     * @return  self
     */ 
    public function setDescriptionope($descriptionope)
    {
        $this->descriptionope = $descriptionope;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of identifiant
     */ 
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * Set the value of identifiant
     *
     * @return  self
     */ 
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    /**
     * Get the value of idrole_connexion
     */ 
    public function getIdrole_connexion()
    {
        return $this->idrole_connexion;
    }

    /**
     * Set the value of idrole_connexion
     *
     * @return  self
     */ 
    public function setIdrole_connexion($idrole_connexion)
    {
        $this->idrole_connexion = $idrole_connexion;

        return $this;
    }

    /**
     * Get the value of mot_de_passe
     */ 
    public function getMot_de_passe()
    {
        return $this->mot_de_passe;
    }

    /**
     * Set the value of mot_de_passe
     *
     * @return  self
     */ 
    public function setMot_de_passe($mot_de_passe)
    {
        $this->mot_de_passe = $mot_de_passe;

        return $this;
    }
}