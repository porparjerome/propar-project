<?php

class Client
{
    public $idclient;
    public static $nomclient;
    public static $prenomclient;
    public $societe;
    
public function __construct($nomclient,$prenomclient,$societe)
{
    $this->nomclient=$nomclient;
    $this->prenomclient=$prenomclient;
    $this->societe=$societe;
}  


    public function getIdclient()
    {
        return $this->idclient;
    }
    public function setIdclient($idclient)
    {
        $this->idclient = $idclient;

        return $this;
    }
    public function getNomclient()
    {
        return $this->nomclient;
    }
    public function setNomclient($nomclient)
    {
        $this->nomclient = $nomclient;

        return $this;
    }
    public function getPrenomclient()
    {
        return $this->prenomclient;
    }
    public function setPrenomclient($prenomclient)
    {
        $this->prenomclient = $prenomclient;

        return $this;
    }
    public function getSociete()
    {
        return $this->societe;
    }
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }
}