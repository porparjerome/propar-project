<?php

class Operation extends Client
{
    
    private $typeope;
    private $descriptionope;
    private $prix;

public function __construct($typeope,$nomclient,$prenomclient,$societe,$descriptionope,$prix)
{
    $this->typeope=$typeope;
    $this->descriptionope=$descriptionope;
    $this->prix=$prix;
    parent::__construct($nomclient,$prenomclient,$societe);

}

    public function getIdope()
    {
        return $this->idope;
    }
    public function setIdope($idope)
    {
        $this->idope = $idope;

        return $this;
    }
    public function getTypeope()
    {
        return $this->typeope;
    }
    public function setTypeope($typeope)
    {
        $this->typeope = $typeope;

        return $this;
    }
    public function getDescriptionope()
    {
        return $this->descriptionope;
    }
    public function setDescriptionope($descriptionope)
    {
        $this->descriptionope = $descriptionope;

        return $this;
    }
    public function getPrix()
    {
        return $this->prix;
    }
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }
}