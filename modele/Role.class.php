<?php

class Role extends Employe
{
    public static $cpt;
    public $grade;
    
    public function __construct($nomemploye,$prenomemploye,$identifiant,$mot_de_passe,$grade)
    {
        self::$cpt = self::$cpt + 1;
        $this->idemploye = self::$cpt;   
        $this->nomemploye=$nomemploye;
        $this->prenomemploye=$prenomemploye;
        $this->identifiant=$identifiant;
        $this->mot_de_passe=$mot_de_passe;
        $this->role=$grade;
    }

    public function getGrade()
    {
        return $this->grade;
    }
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }
}