<?php
include_once 'singleton.class.php';
class Employe
{
   /*  public static $cpt; */
    private $nomemploye;
    private $prenomemploye; 
    private $identifiant;
    private $mot_de_passe;

    public function __construct($nomemploye,$prenomemploye,$identifiant,$mot_de_passe)
    {
        /* self::$cpt = self::$cpt + 1;
        $this->idemploye = self::$cpt; */   
        $this->nomemploye=$nomemploye;
        $this->prenomemploye=$prenomemploye;
        $this->identifiant=$identifiant;
        $this->mot_de_passe=$mot_de_passe;
        
    }
    /**
         * connect function
         * permet de verfier sur l'identifiant et le mot de passe sont bon pour se connecté
         * @return bool
         */
        public function connect(){
            $dbi = Singleton::getInstance();
            $db=$dbi->getConnection();
            $resultat = $db->query('SELECT identifiant, mot_de_passe from employe');    
            $cpt=0;            
            $id =$this->checkidentifiant();
            if ($id>=0) 
            {
               return $this->checkmotdepasse($id);
            }
            else
            {
                return false;
            }
        }
                /**
                 * checkEmail function
                 *verifie si l'email est dans la base de donnée
                 * @return bool
                 */
        public function checkidentifiant(){
            $dbi = Singleton::getInstance();
            $db=$dbi->getConnection();
            $resultat = $db->query('SELECT identifiant from employe');
            $i=0;
            foreach($resultat as $row){
                $i++;
                $arr[$i] = $row['identifiant'];
            }
            if(in_array($this->identifiant,$arr)){
                return $i;
            }
            else{
                return -1 ;
            }
        }
        public function checkmotdepasse($j){
            $dbi = Singleton::getInstance();
            $db=$dbi->getConnection();
            $resultat = $db->query('SELECT mot_de_passe from employe');
            $i=0;
            foreach($resultat as $row){
                $i++;
                $arr[$i] = $row['mot_de_passe'];
            }
            if($arr=[$j]=$this->mot_de_passe){
                return true;
            }
            else{
                return false;
            }
        }
             /**
             * Get the value of identifiant
             */ 
            public function get_identifiant()
            {               
                $db = new PDO('mysql:host=localhost;dbname=propar_project', "root", "");
                $resultat = $db->query("SELECT identifiant from employe where identifiant='$this->identifiant'");
                $arr = $resultat->fetchall(PDO::FETCH_COLUMN);
                $identifiant=$arr[0];
                return $this->identifiant = $identifiant;
            }           
            /**
             * Set the value of identifiant
             *
             * @return  self
             */ 
            public function set_identifiant($identifiant)
            {
                        $this->identifiant = $identifiant;

                        return $this;
            }

             /**
             * Get the value of motdepasse
             */ 
            public function get_mot_de_passe()
            {               
                $db = new PDO('mysql:host=localhost;dbname=propar_project', "root", "");
                $resultat = $db->query("SELECT mot_de_passe from employe where mot_de_passe='$this->mot_de_passe'");
                $arr = $resultat->fetchall(PDO::FETCH_COLUMN);
                $identifiant=$arr[0];
                return $this->identifiant = $identifiant;
            }           
            /**
             * Set the value of motdepasse
             *
             * @return  self
             */ 
            public function set_mot_de_passe($mot_de_passe)
            {
                        $this->mot_de_passe = $mot_de_passe;

                        return $this;
            }

            public function get_idrole()
            {
                $db = new PDO('mysql:host=localhost;dbname=propar_project', "root", "");
                $resultat = $db->query("SELECT idrole from employe where identifiant='$this->identifiant'");
                $arr = $resultat->fetchall(PDO::FETCH_COLUMN);
                $idrole=$arr[0];
                return $idrole; 
            }







    public function getIdemploye()
    {
        return $this->idemploye;
    }
    public function setIdemploye($idemploye)
    {
        $this->idemploye = $idemploye;

        return $this;
    }
    public function getNomemploye()
    {
        return $this->nomemploye;
    }
    public function setNomemploye($nomemploye)
    {
        $this->nomemploye = $nomemploye;

        return $this;
    }
    public function getPrenomemploye()
    {
        return $this->prenomemploye;
    }
    public function setPrenomemploye($prenomemploye)
    {
        $this->prenomemploye = $prenomemploye;

        return $this;
    }
    public function getIdentifiant()
    {
        return $this->identifiant;
    }
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;

        return $this;
    }
    public function getMot_de_passe()
    {
        return $this->mot_de_passe;
    }
    public function setMot_de_passe($mot_de_passe)
    {
        $this->mot_de_passe = $mot_de_passe;

        return $this;
    }
}