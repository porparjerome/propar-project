#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: role
#------------------------------------------------------------

CREATE TABLE role(
        idrole Int  Auto_increment  NOT NULL ,
        grade  Varchar (50) NOT NULL
	,CONSTRAINT role_PK PRIMARY KEY (idrole)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: employe
#------------------------------------------------------------

CREATE TABLE employe(
        idemploye     Int  Auto_increment  NOT NULL ,
        nomemploye    Varchar (50) NOT NULL ,
        prenomemploye Varchar (50) NOT NULL ,
        identifiant   Varchar (10) NOT NULL ,
        mot_de_passe  Varchar (10) NOT NULL ,
        idrole        Int NOT NULL
	,CONSTRAINT employe_PK PRIMARY KEY (idemploye)

	,CONSTRAINT employe_role_FK FOREIGN KEY (idrole) REFERENCES role(idrole)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: client
#------------------------------------------------------------

CREATE TABLE client(
        idclient     Int  Auto_increment  NOT NULL ,
        nomclient    Varchar (50) NOT NULL ,
        prenomclient Varchar (50) NOT NULL ,
        societe      Varchar (50) NOT NULL
	,CONSTRAINT client_PK PRIMARY KEY (idclient)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: operation
#------------------------------------------------------------

CREATE TABLE operation(
        idoperation    Int  Auto_increment  NOT NULL ,
        typeope        Varchar (50) NOT NULL ,
        descriptionope Varchar (150) NOT NULL ,
        prix           Int NOT NULL ,
        idemploye      Int NOT NULL ,
        idclient       Int NOT NULL
	,CONSTRAINT operation_PK PRIMARY KEY (idoperation)

	,CONSTRAINT operation_employe_FK FOREIGN KEY (idemploye) REFERENCES employe(idemploye)
	,CONSTRAINT operation_client0_FK FOREIGN KEY (idclient) REFERENCES client(idclient)
)ENGINE=InnoDB;

