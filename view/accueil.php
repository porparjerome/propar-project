<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Commande</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        @import url("../assets/styles.css");
    </style>
</head>

<body>
    <div>
        <img src="..\assets\logo-propar.png" alt="" >
        
    </div>
        <div class="table table-sm" >
            <table style="width:60%;text-align:center;background-color: lime" >
            <!-- <caption>opération en cours</caption> -->
                <thead class="thead-dark">
                    <tr>
                    <th>Opérations en cours</th>
                    <th>Clients</th>
                     <th>Employé</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include '../modele/bdd.class.php';
                    $pdo = Singleton::getInstance();
                    $db=$pdo->getconnection();
                    $sql=$db->query('select e.nomemploye,o.typeope, n.nomclient from employe as e 
                    join operation as o on o.idemploye=e.idemploye join client as n on o.idclient=n.idclient');
                   
                    foreach ($sql as $row)
                    {
                echo '<tr>';
                echo '<td>'.$row['typeope'].'</td>';
                echo '<td>'.$row['nomclient'].'</td>';
                echo '<td>'.$row['nomemploye'].'</td>';
                echo '</tr>';
                
                    }
                ?>
                </tbody>
            </table>
        </div>
        <div class="table table-sm">
            <table style="width:60%;text-align:center;background-color: lime">
            <!-- <caption>opérations terminées</caption> -->
                <thead class="thead-dark">
                    <tr>
                    <th>Opérations terminées</th>
                    <th>Clients</th>
                    <th>Employé</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                <td>exemple ope</td>
                <td>exemple client</td>
                <td>exemple employe</td>
                </tr>
                <tr>
                <td>exemple ope</td>
                <td>exemple client</td>
                <td>exemple employe</td>
                </tr>
                <tr>
                <td>exemple ope</td>
                <td>exemple client</td>
                <td>exemple employe</td>
                </tr>
                </tbody>
            </table>
            <br/>
        </div>
            <div class="col mb-2 nopadding">
                <div class="row">
                    <div class="col-sm-12 offset-6 col-md-5 text-right nopadding">
                        <!-- <button class="btn btn-lg btn-block btn-success text-uppercase">Connectez-vous</button> -->
                        <button onclick="window.location.href = 'connexion.php';">Connectez-vous</button>
                    </div>
                </div>
            </div>

</body>
</html>